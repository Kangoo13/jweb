<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
             <div class="column col-sm-9" id="main">
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->
                        
                        <div class="col-sm-12" id="featured">   
                          <div class="page-header text-muted">
                          News
                          </div> 
                        </div>
                        <c:forEach items="${ news }" var="mapNews" varStatus="boucle">
                        <!--/top story-->
                        <div class="row">    
                          <div class="col-sm-10">
                            <h3><c:out value="${ mapNews.value.titre }"/></h3>
                            <h4><c:out value="${ mapNews.value.texte }"/></h4><h4>
                            <small class="text-muted"><c:out value="${ mapNews.value.date }"/> <a href="#" class="text-muted"><c:out value="${ mapNews.value.auteur }"/></a></small>
                            </h4>
                          </div>
                        </div><br />

                        </c:forEach>
</div> </div>                        </div>
  