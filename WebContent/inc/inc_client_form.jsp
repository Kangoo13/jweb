<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<label for="nom">Nom </label>
<input type="text" id="nom" name="nom" value="<c:out value="${client.nom}"/>" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['nom']}</span>
<br />

<label for="prenom">Pr�nom </label>
<input type="text" id="prenom" name="prenom" value="<c:out value="${client.prenom}"/>" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['prenom']}</span>
<br />

<label for="password">Password </label>
<input type="password" id="password" name="password" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['password']}</span>
<br />

<label for="password2">Password (confirmation)</label>
<input type="password" id="password2" name="password2" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['password2']}</span>
<br />

<label for="cp">Code postal </label>
<input type="text" id="cp" name="cp" value="<c:out value="${client.cp}"/>" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['cp']}</span>
<br />

<label for="pays">Pays </label>
<input type="text" id="pays" name="pays" value="<c:out value="${client.pays}"/>" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['pays']}</span>
<br />

<label for="adresse">Adresse de livraison</label>
<input type="text" id="adresse" name="adresse" value="<c:out value="${client.adresse}"/>" size="30" maxlength="60" />
<span class="erreur">${form.erreurs['adresse']}</span>
<br />

<label for="telephone">Num�ro de t�l�phone </label>
<input type="text" id="telephone" name="telephone" value="<c:out value="${client.telephone}"/>" size="30" maxlength="30" />
<span class="erreur">${form.erreurs['telephone']}</span>
<br />

<label for="email">Adresse email</label>
<input type="email" id="email" name="email" value="<c:out value="${client.email}"/>" size="30" maxlength="60" />
<span class="erreur">${form.erreurs['email']}</span>
<br />