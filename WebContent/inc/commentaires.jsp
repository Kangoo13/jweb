<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
                <table>
                <tr>
                    <th>Titre</th>
                    <th>Note</th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>Date</th>                 
                </tr>
                <%-- Parcours de la Map des clients en session, et utilisation de l'objet varStatus. --%>
                <c:forEach items="${ commentaires }" var="commentaires" varStatus="boucle">
                <%-- Simple test de parit� sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
                <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                    <%-- Affichage des propri�t�s du bean Client, qui est stock� en tant que valeur de l'entr�e courante de la map --%>
                    <td><c:out value="${ commentaires.titre }"/></td>
                    <td><c:out value="${ commentaires.note }"/></td>
                    <td><c:out value="${ commentaires.client.nom }"/></td>                    
                    <td><c:out value="${ commentaires.description }"/></td>
                    <td><c:out value="${ commentaires.date }"/></td>
                                    </c:forEach>
                                    </table>