<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <table>
                <tr>
                    <th>Auteur</th>
                    <th>Titre</th>
                    <th>Date</th>
                    <th>Texte</th>
                    <th></th>
                    <th></th>             
                </tr>
                <%-- Parcours de la Map des clients en session, et utilisation de l'objet varStatus. --%>
                <c:forEach items="${ news }" var="mapNews" varStatus="boucle">
                <%-- Simple test de parit� sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
                <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                    <%-- Affichage des propri�t�s du bean Client, qui est stock� en tant que valeur de l'entr�e courante de la map --%>
                    <td><c:out value="${ mapNews.value.auteur }"/></td>
                    <td><c:out value="${ mapNews.value.titre }"/></td>
                    <td><c:out value="${ mapNews.value.date }"/></td>
                    <td><c:out value="${ mapNews.value.texte }"/></td>
                    <td><a href="${pageContext.servletContext.contextPath}/admin/modifierNews?id=${ mapNews.value.id }">Modifier</a></td>                    
                    <td><a href="${pageContext.servletContext.contextPath}/admin/supprimerNews?id=${ mapNews.value.id }">Supprimer</a></td>                    
                </c:forEach>
              </table>