<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <table>
                <tr>
                    <th>Nom produit</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Prix</th>                 
                </tr>
                <%-- Parcours de la Map des clients en session, et utilisation de l'objet varStatus. --%>
                <c:forEach items="${ produits }" var="mapProduits" varStatus="boucle">
                <%-- Simple test de parit� sur l'index de parcours, pour alterner la couleur de fond de chaque ligne du tableau. --%>
                <tr class="${boucle.index % 2 == 0 ? 'pair' : 'impair'}">
                    <%-- Affichage des propri�t�s du bean Client, qui est stock� en tant que valeur de l'entr�e courante de la map --%>
                    <td><a href ="${pageContext.servletContext.contextPath}/detailProduit?id=<c:out value="${ mapProduits.value.id }"/>"><c:out value="${ mapProduits.value.name }"/></a></td>
                    <td><c:out value="${ mapProduits.value.description }"/></td>
                    <td><c:out value="${ mapProduits.value.image }"/></td>
                    <td><c:out value="${ mapProduits.value.price }"/></td>
                </c:forEach>
             </table>