<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>
	    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/wow.min.js"></script>
   <header id="header">
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="/accueil">Accueil</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/listerProduits">Produits</a></li> 
                        <c:choose> 
  <c:when test="${empty user.nom}">
<li><a href="${pageContext.servletContext.contextPath}/inscriptionClient" >Inscription</a></li> 
<li><a href="${pageContext.servletContext.contextPath}/connexionClient" >Connexion</a></li> 
  </c:when>
  <c:otherwise>
<li><a href="${pageContext.servletContext.contextPath}/logout">Se déconnecter</a></li> 
                        <c:choose> 
  <c:when test="${user.admin == 1}">
  <li><a href="${pageContext.servletContext.contextPath}/admin/accueil">Administration</a></li> 
  </c:when>
                          </c:choose> 
  </c:otherwise>
</c:choose>      
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        </header>
