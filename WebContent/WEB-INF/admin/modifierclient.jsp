<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Modification d'un client</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
        <c:import url="/inc/menu.jsp" />
		<form action="${pageContext.servletContext.contextPath}/admin/modifierClientPost?id=${client.id}" method="POST">
		<input type="hidden" name="id" value="${ client.id }">
                Nom :<br>
        <input type="text" name="nom" value="${ client.nom }"><span class="erreur">${form.erreurs['nom']}</span><br />
                Prenom :<br>
        <input type="text" name="prenom" value="${ client.prenom }"><span class="erreur">${form.erreurs['prenom']}</span><br />
                        Adresse :<br>
        <input type="text" name="adresse" value="${ client.adresse }"><span class="erreur">${form.erreurs['adresse']}</span><br />
                        Telephone :<br>
        <input type="text" name="telephone" value="${ client.telephone }"><span class="erreur">${form.erreurs['telephone']}</span><br />
                        Email :<br>
        <input type="text" name="email" value="${ client.email }"><span class="erreur">${form.erreurs['email']}</span><br />
                        Password :<br>
        <input type="password" name="password"><span class="erreur">${form.erreurs['password']}</span><br />
                        Code postal :<br>
        <input type="text" name="cp" value="${ client.cp }"><span class="erreur">${form.erreurs['cp']}</span><br />
                        Pays :<br>
        <input type="text" name="pays" value="${ client.pays }"><span class="erreur">${form.erreurs['pays']}</span><br />
                        Admin :<br>
	<SELECT name="admin">
		<c:choose>
  <c:when test="${client.admin == 0}">
		<OPTION VALUE="0" selected>0</OPTION>
		<OPTION VALUE="1">1</OPTION>
  </c:when>
  <c:otherwise>
		<OPTION VALUE="0">0</OPTION>
		<OPTION VALUE="1" selected>1</OPTION>
  </c:otherwise>
</c:choose>

	</SELECT>
                        Newsletter :<br>
	<SELECT name="newsletter">
	<c:choose>
  <c:when test="${client.newsletter == 0}">
		<OPTION VALUE="0" selected>0</OPTION>
		<OPTION VALUE="1">1</OPTION>
  </c:when>
  <c:otherwise>
		<OPTION VALUE="0">0</OPTION>
		<OPTION VALUE="1" selected>1</OPTION>
  </c:otherwise>
</c:choose>
	</SELECT>
	<br />
<input type="submit" />
</form>
                <p class="info">${ form.resultat }</p>
        <c:import  url="/inc/footer.jsp" />
    </body>
</html>