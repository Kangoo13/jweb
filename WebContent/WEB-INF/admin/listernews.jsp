<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Liste des produits</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
        <c:import url="/inc/menu.jsp" />
        <div>
        		<%@ include file="/inc/admin/listernews.jsp"%>
        </div>
        Ajouter une news : 
		<form action="${pageContext.servletContext.contextPath}/admin/ajouterNews" method="POST">
                Titre :<br>
        <input type="text" name="titre"><br />
                Contenu :<br>
<textarea name="contenu" cols="50" rows="5" >
</textarea>
<br />
<input type="submit" />
</form>
        <c:import  url="/inc/footer.jsp" />
    </body>
</html>