<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Connexion</title>
        
        
        <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">

    
    <script src="http://getbootstrap.com/examples/signin/signin.css"></script>
        
        
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
                            <c:import url="/inc/menu.jsp" />
        <div class="container">
            <form  class="form-signin"  method="post" action="<c:url value="/connexionClient"/>">
               <fieldset>
               <h2 class="form-signin-heading">Please sign in</h2>
                    <legend>Informations client</legend>
                    <label for="email">E-mail <span class="srl-only">*</span></label>
					<input type="text" id="email" name="email" value="<c:out value="${client.email}"/>" size="30" maxlength="30" />
					<label for="password">Password <span class="requis">*</span></label>
					<input type="password" id="password" name="password" value="<c:out value="${client.password}"/>" size="30" maxlength="30" />
				<br />
				<br />
                </fieldset>
                <p class="info">${ form.resultat }</p>
                <button class="btn btn-lg btn-primary btn-block" type="submit" value="Valider">Sign in </button>
                <input type="reset" class="btn btn-primary" value="Remettre � z�ro" /> <br />
            </form>
        </div>
        <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
       <c:import  url="/inc/footer.jsp" />
    </body>
</html>