<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>D�tail d'un produit</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/inc/style.css"/>" />
    </head>
    <body>
        <c:import url="/inc/menu.jsp" />
        <div>
        		 <table>
                <tr>
                    <th>Nom produit</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Prix</th>                 
                </tr>
                <tr>
                    <td><c:out value="${ produit.name }"/></td>
                    <td><c:out value="${ produit.description }"/></td>
                    <td><c:out value="${ produit.image }"/></td>
                    <td><c:out value="${ produit.price }"/></td>
             </table>
        </div><br />
        <c:if test="${not empty param.id && not empty user.id}">
		<form action="${pageContext.servletContext.contextPath}/nouveauCommentaire?id=${param.id}" method="POST">
        Note :<br>
		<input type="radio" name="note" value="1">1
		<input type="radio" name="note" value="2">2
		<input type="radio" name="note" value="3" checked>3
		<input type="radio" name="note" value="4">4
		<input type="radio" name="note" value="5">5<br />
                Titre :<br>
        <input type="text" name="titre"><br />
                Contenu :<br>
<textarea name="contenu" cols="50" rows="5">
Votre avis
</textarea>
<br />
<input type="submit" />
</form>
		</c:if>

		        <c:import url="/inc/commentaires.jsp" />
        <c:import  url="/inc/footer.jsp" />
    </body>
</html>