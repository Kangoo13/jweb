package com.jweb.beans;

import java.sql.Date;

public class Commentaire {
	
		private long idProduit;
		private String titre;
		private int note;
		private long idMembre;
		private Client client;
		private String description;
		private Date date;
		
		public String getTitre() {
			return titre;
		}
		public void setTitre(String titre) {
			this.titre = titre;
		}
		
		public int getNote() {
			return note;
		}
		public void setNote(int note) {
			this.note = note;
		}
		public long getIdProduit() {
			return idProduit;
		}
		public void setIdProduit(long idProduit) {
			this.idProduit = idProduit;
		}
		public long getIdMembre() {
			return idMembre;
		}
		public void setIdMembre(long idMembre) {
			this.idMembre = idMembre;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public Client getClient() {
			return client;
		}
		public void setClient(Client client) {
			this.client = client;
		}
}
