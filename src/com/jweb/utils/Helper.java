package com.jweb.utils;

import javax.servlet.http.HttpServletRequest;

import com.jweb.beans.Client;

public final class Helper {
	public static final String CONF_DAO_FACTORY = "daofactory";
	
    public static final String VUE_ADMIN_ACCUEIL = "/WEB-INF/admin/admin.jsp";
    public static final String VUE_ACCUEIL = "/accueil";
    public static final String VUE_ACCUEIL_JSP = "/WEB-INF/accueil.jsp";
    public static final String VUE_CONNEXION_JSP   = "/WEB-INF/connexion.jsp";
    public static final String VUE_INSCRIPTION_JSP   = "/WEB-INF/inscription.jsp";
    public static final String VUE_PRODUITS = "/listerProduits";
    public static final String VUE_LISTER_PRODUITS = "/listerProduits";
    public static final String VUE_LISTER_PRODUITS_JSP = "/WEB-INF/produitlist.jsp";
    public static final String VUE_PRODUIT_DETAIL_JSP = "/WEB-INF/detailproduit.jsp";
    public static final String VUE_PRODUIT_DETAIL = "/detailProduit";
    public static final String VUE_ADMIN_NEWS = "/admin/gestionNews";
    public static final String VUE_ADMIN_CLIENTS = "/admin/gestionClients";
    public static final String VUE_ADMIN_LISTER_CLIENTS = "/WEB-INF/admin/listerutilisateurs.jsp";
    public static final String VUE_ADMIN_LISTER_NEWS = "/WEB-INF/admin/listernews.jsp";
    public static final String VUE_ADMIN_MODIFIER_CLIENT = "/WEB-INF/admin/modifierclient.jsp";
    public static final String VUE_ADMIN_MODIFIER_NEWS = "/WEB-INF/admin/modifiernews.jsp";
    
    public static final String ATT_CLIENTS = "clients";
    public static final String ATT_CLIENT = "client";
    public static final String ATT_PRODUIT = "produit";
    public static final String ATT_PRODUITS = "produits";
    public static final String ATT_COMMENTAIRES = "commentaires";
    public static final String ATT_NEWS = "news";
    public static final String ATT_FORM = "form";
    public static final String ATT_NEWSLETTER_FORM   = "formNewsletter";
    
    public static final String CHAMP_ID		= "id";
	public static final String CHAMP_NOM       = "nom";
    public static final String CHAMP_PRENOM    = "prenom";
    public static final String CHAMP_PASSWORD    = "password";
    public static final String CHAMP_PASSWORDC    = "password2";
    public static final String CHAMP_CP    = "cp";
    public static final String CHAMP_PAYS    = "pays";
    public static final String CHAMP_ADRESSE   = "adresse";
    public static final String CHAMP_TELEPHONE = "telephone";
    public static final String CHAMP_EMAIL     = "email";
    public static final String CHAMP_NOTE     = "note";
    public static final String CHAMP_TITRE     = "titre";
    public static final String CHAMP_CONTENU     = "contenu";
    public static final String CHAMP_NEWSLETTER     = "newsletter";
    public static final String CHAMP_ADMIN     = "admin";
    
    
    
    /* Conversion d'une cha�ne de caract�res en MD5 */
	static public String MD5(String md5) {
		   try {
		        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		        byte[] array = md.digest(md5.getBytes());
		        StringBuffer sb = new StringBuffer();
		        for (int i = 0; i < array.length; ++i) {
		          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
		       }
		        return sb.toString();
		    } catch (java.security.NoSuchAlgorithmException e) {
		    }
		    return null;
		}
	
	
	/* Fonction pour v�rifier si l'utilisateur est un admin */
	static public Client isAdmin(HttpServletRequest request)
	{
    	Object user = request.getSession().getAttribute("user");
		Client client = (Client) user;
    	if (user == null || client.getAdmin() == 0) 
    		return null;
    	return client;
	}
}
