package com.jweb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsletterDao;
import com.jweb.forms.FM_ajouterClientNewsletter;
import com.jweb.utils.Helper;

@WebServlet(value="/ajouterClientNewsletter", name="ajouterClientNewsletter")
public class SV_ajouterClientNewsletter extends HttpServlet {

	private static final long serialVersionUID = 5926999954061054631L;

    private NewsletterDao     newsletterDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO Newsletter */
        this.newsletterDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsletterDao();
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Pr�paration de l'objet formulaire */
        FM_ajouterClientNewsletter formNewsletter = new FM_ajouterClientNewsletter(newsletterDao);

        /* Traitement de la requ�te */
        formNewsletter.ajouterNewsletter(request);

        /* Ajout de l'objet m�tier � l'objet requ�te */
        request.setAttribute( Helper.ATT_NEWSLETTER_FORM, formNewsletter );

            /* affichage de l'accueil */
        this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    }
}
