package com.jweb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.utils.Helper;

@WebServlet(value="/logout", name="logout")
public class SV_logout extends HttpServlet {
	private static final long serialVersionUID = 8813664084663975687L;
	
	public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	/* suppression de la session puis redirection vers l'accueil */
    	   request.getSession().invalidate();
           this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward(request, response);;
    }
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	/* suppression de la session puis redirection vers l'accueil */
    	   request.getSession().invalidate();
           this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward(request, response);;
    }
}
