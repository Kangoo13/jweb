package com.jweb.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Produit;
import com.jweb.dao.DAOFactory;
import com.jweb.dao.ProduitDao;
import com.jweb.forms.FM_listerProduits;
import com.jweb.utils.Helper;

@WebServlet(value="/listerProduits", name="listerProduits")
public class SV_listerProduits extends HttpServlet {

	private static final long serialVersionUID = -8943240919934275044L;

    private ProduitDao     produitDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO Produit */
        this.produitDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getProduitDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	/* R�cup�ration de tous les produits */
    	Map<Long, Produit> mapProduits = new FM_listerProduits(produitDao).recupererProduits(request);	
 
    	/* Ajout des produits � la request */
    	request.setAttribute( Helper.ATT_PRODUITS, mapProduits );
		this.getServletContext().getRequestDispatcher( Helper.VUE_LISTER_PRODUITS_JSP ).forward( request, response );
	 }
    
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	/* R�cup�ration de tous les produits */
    	Map<Long, Produit> mapProduits = new FM_listerProduits(produitDao).recupererProduits(request);	
 
    	/* Ajout des produits � la request */
    	request.setAttribute( Helper.ATT_PRODUITS, mapProduits );
		this.getServletContext().getRequestDispatcher( Helper.VUE_LISTER_PRODUITS_JSP ).forward( request, response );
	 }
}
