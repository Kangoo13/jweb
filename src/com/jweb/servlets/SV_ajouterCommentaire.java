package com.jweb.servlets;

import java.io.IOException;




import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




import com.jweb.dao.CommentaireDao;
import com.jweb.dao.DAOFactory;
import com.jweb.forms.FM_ajouterCommentaire;
import com.jweb.utils.Helper;

@WebServlet(value="/ajouterCommentaire", name="ajouterCommentaire")
public class SV_ajouterCommentaire extends HttpServlet {

	private static final long serialVersionUID = -5595340716282691279L;

    private CommentaireDao     commentaireDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Commentaire */
        this.commentaireDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getCommentaireDao();
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        FM_ajouterCommentaire form = new FM_ajouterCommentaire(commentaireDao);

        /* Ajout du commentaire */
        form.ajouterCommentaire(request);
        
        /* Récupération de l'id pour réafficher le produit */
        String id = request.getParameter("id");
        
        /* Affichage de la fiche récapitulative */
        this.getServletContext().getRequestDispatcher( Helper.VUE_PRODUIT_DETAIL + id ).forward( request, response );

    }
}
