package com.jweb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOFactory;
import com.jweb.forms.FM_connexionClient;
import com.jweb.utils.Helper;

@WebServlet(value="/connexionClient", name="connexionClient")
public class SV_connexionClient extends HttpServlet {

	private static final long serialVersionUID = 5926999954091054631L;

    private ClientDao     clientDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO Client */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getClientDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* � la r�ception d'une requ�te GET, simple affichage du formulaire */
        this.getServletContext().getRequestDispatcher( Helper.VUE_CONNEXION_JSP ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Pr�paration de l'objet formulaire */
        FM_connexionClient form = new FM_connexionClient(clientDao);

        /* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
        Client client = form.creerClient( request );

        /* Ajout du bean et de l'objet m�tier � l'objet requ�te */
        request.setAttribute( Helper.ATT_CLIENT, client );
        request.setAttribute( Helper.ATT_FORM, form );

        if ( form.getResultat().matches("Succ�s de la cr�ation du client.") ) {
        	/* Ajout de l'utilisateur en session */
        	request.getSession().setAttribute("user", client);
            /* Si aucune erreur, alors affichage de la fiche r�capitulative */
            this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
        } else {
            /* Sinon, r�-affichage du formulaire de cr�ation avec les erreurs */
            this.getServletContext().getRequestDispatcher( Helper.VUE_CONNEXION_JSP ).forward( request, response );
        }
    }
}
