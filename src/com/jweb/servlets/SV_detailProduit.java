package com.jweb.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Commentaire;
import com.jweb.beans.Produit;
import com.jweb.dao.ClientDao;
import com.jweb.dao.CommentaireDao;
import com.jweb.dao.DAOFactory;
import com.jweb.dao.ProduitDao;
import com.jweb.forms.FM_trouverProduit;
import com.jweb.utils.Helper;

@WebServlet(value="/detailProduit", name="detailProduit")
public class SV_detailProduit extends HttpServlet {

	private static final long serialVersionUID = 7712142611542227384L;


    private ProduitDao     produitDao;
    private CommentaireDao     commentaireDao;
    private ClientDao     clientDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO Produit */
        this.produitDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getProduitDao();
        /* R�cup�ration d'une instance de notre DAO Commentaire */
        this.commentaireDao = ( (DAOFactory) getServletContext().getAttribute(  Helper.CONF_DAO_FACTORY ) ).getCommentaireDao();
        /* R�cup�ration d'une instance de notre DAO Client */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute(  Helper.CONF_DAO_FACTORY ) ).getClientDao();
    }
    
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Pr�paration de l'objet formulaire */
        FM_trouverProduit form = new FM_trouverProduit(produitDao, commentaireDao, clientDao);

        /* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
        Produit produit = form.trouver(request);
        
        /* R�cup�ration de la liste des commentaires du produit */
        List<Commentaire> commentaires = form.trouverCommentaires(request);
        
        /* Ajout du produit � l'objet requ�te et aussi des commentaires*/
        if (produit != null)
        	request.setAttribute(  Helper.ATT_PRODUIT, produit );
        if (commentaires != null)
        	request.setAttribute(  Helper.ATT_COMMENTAIRES, commentaires);
        
        /* Affichage de la fiche r�capitulative */
        this.getServletContext().getRequestDispatcher(  Helper.VUE_PRODUIT_DETAIL_JSP ).forward( request, response );
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Pr�paration de l'objet formulaire */
        FM_trouverProduit form = new FM_trouverProduit(produitDao, commentaireDao, clientDao);

        /* Traitement de la requ�te et r�cup�ration du bean en r�sultant */
        Produit produit = form.trouver(request);
        
        /* R�cup�ration de la liste des commentaires du produit */
        List<Commentaire> commentaires = form.trouverCommentaires(request);
        
        /* Ajout du produit � l'objet requ�te et aussi des commentaires*/
        if (produit != null)
        	request.setAttribute(  Helper.ATT_PRODUIT, produit );
        if (commentaires != null)
        	request.setAttribute(  Helper.ATT_COMMENTAIRES, commentaires);
        
        /* Affichage de la fiche r�capitulative */
        this.getServletContext().getRequestDispatcher(  Helper.VUE_PRODUIT_DETAIL_JSP ).forward( request, response );
    }
}
