package com.jweb.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.News;
import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsDao;
import com.jweb.forms.FM_accueilNews;
import com.jweb.utils.Helper;

@WebServlet(value="/accueil", name="accueil")
public class SV_afficherAccueil extends HttpServlet {

	private static final long serialVersionUID = 896688523908038179L;

    private NewsDao     newsDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO News */
        this.newsDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsDao();
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
    	FM_accueilNews form = new FM_accueilNews(newsDao);
    	
    	/* Récupération de toutes les news dans une map */
    	Map<Long, News> mapNews = form.recupererNews(request);
    	
    	/* Ajout de la map dans la request */
        request.setAttribute( Helper.ATT_NEWS, mapNews );
        
        /* Affichage de l'index (accueil) */
        this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL_JSP ).forward( request, response );
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
    	FM_accueilNews form = new FM_accueilNews(newsDao);
    	
    	/* Récupération de toutes les news dans une map */
    	Map<Long, News> mapNews = form.recupererNews(request);
    	
    	/* Ajout de la map dans la request */
        request.setAttribute( Helper.ATT_NEWS, mapNews );
        
        /* Affichage de l'index (accueil) */
        this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL_JSP ).forward( request, response );
    }
}
