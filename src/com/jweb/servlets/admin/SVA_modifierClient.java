package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOFactory;
import com.jweb.forms.admin.FMA_modifierClient;
import com.jweb.utils.Helper;


@WebServlet(value="/admin/modifierClient", name="modifierClient")
public class SVA_modifierClient extends HttpServlet {

	private static final long serialVersionUID = -8932180040045308036L;

	private ClientDao     clientDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO Client */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getClientDao();
    }
    
   
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* Cr�ation de l'objet formulaire */
    		FMA_modifierClient form = new FMA_modifierClient(clientDao);
    		
    		/* Modification du client et r�cup�ration de l'objet */
    		Client client = form.modifierClient(request);
    		
    		/* Si il est pas null, ajout du client � la request et forward vers le formulaire de modification */
    		if (client!= null)
    		{
    			request.setAttribute( Helper.ATT_CLIENT, client );
    			this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_MODIFIER_CLIENT ).forward( request, response );
    		}
    		else
    			this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_ACCUEIL ).forward( request, response );
    	}
    }
}
