package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsDao;

import com.jweb.forms.admin.FMA_supprimerNews;

import com.jweb.utils.Helper;

@WebServlet(value="/admin/supprimerNews", name="supprimerNews")
public class SVA_supprimerNews extends HttpServlet {

	private static final long serialVersionUID = 896688523908038179L;

    private NewsDao     newsDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.newsDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsDao();
    }
        
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
	    	/* Création de l'objet formulaire supprimer news */
	    	FMA_supprimerNews form = new FMA_supprimerNews(newsDao);
	
	    	/* Suppression de la news */
	        form.supprimerNews(request);
	        
	        /* Affichage des news */
	        this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_NEWS ).forward( request, response );
	    }
    }
}
