package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.utils.Helper;

@WebServlet(value="/admin/accueil", name="accueilAdmin")
public class SVA_afficherAccueil extends HttpServlet {

	private static final long serialVersionUID = -3032831193096401086L;


	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
     	else
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_ACCUEIL ).forward( request, response );
    }
    
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_ACCUEIL ).forward( request, response );
    }
}
