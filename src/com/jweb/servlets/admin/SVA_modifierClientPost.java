package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOFactory;
import com.jweb.forms.admin.FMA_modifierClientPost;
import com.jweb.utils.Helper;

@WebServlet(value="/admin/modifierClientPost", name="modifierClientPost")
public class SVA_modifierClientPost extends HttpServlet {


	private static final long serialVersionUID = -5709931715174247722L;
	
	private ClientDao     clientDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Client */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getClientDao();
    }
    
   
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else{
    		/* Création du formulaire pour modifier un client */
    		FMA_modifierClientPost form = new FMA_modifierClientPost(clientDao);

    		/* Modification du client */
	        form.modifierClient(request);
	        /* Si il y a des erreurs, réaffichage du formulaire, sinon affichage des clients */
	        if ( form.getErreurs().isEmpty() ) {
	            this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_CLIENTS ).forward( request, response );
	        } else {
	        	/* Récupération de l'id pour trouver le client et le remettre dans le formulaire */
	            Long id = Long.valueOf(request.getParameter("id"));
	            Client client = clientDao.trouver(id);
	            
	            /* Ajout des attributs dans la requête, le client et le message d'erreur */
	            request.setAttribute(Helper.ATT_CLIENT, client);
	            request.setAttribute(Helper.ATT_FORM, form);

	            this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_MODIFIER_CLIENT).forward( request, response );
	        }
    	}
    }
}
