package com.jweb.servlets.admin;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOFactory;
import com.jweb.forms.admin.FMA_gestionClients;
import com.jweb.utils.Helper;

@WebServlet(value="/admin/gestionClients", name="gestionClients")
public class SVA_gestionClients extends HttpServlet {

	private static final long serialVersionUID = 7355632445229778828L;

	private ClientDao     clientDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Client */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getClientDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* Récupération des clients */
            Map<Long, Client> mapClients = new FMA_gestionClients(clientDao).recupererClients(request);	

            /* Ajout des clients a la request */
            request.setAttribute( Helper.ATT_CLIENTS, mapClients );
 
            /* Affichage des clients */
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_LISTER_CLIENTS ).forward( request, response );
    	}
    }
    
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* Récupération des clients */
            Map<Long, Client> mapClients = new FMA_gestionClients(clientDao).recupererClients(request);	
        
            /* Ajout des clients a la request */
            request.setAttribute( Helper.ATT_CLIENTS, mapClients );
         
            /* Affichage des clients */
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_LISTER_CLIENTS ).forward( request, response );
    	}
    }
}
