package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.News;

import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsDao;

import com.jweb.forms.admin.FMA_modifierNews;

import com.jweb.utils.Helper;

@WebServlet(value="/admin/modifierNews", name="modifierNews")
public class SVA_modifierNews extends HttpServlet {

  	private static final long serialVersionUID = -2403062426429822862L;

  	private NewsDao     newsDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO News */
        this.newsDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsDao();
    }
    
   
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* Création de l'objet formulaire pour modifier les news */
	    	FMA_modifierNews form = new FMA_modifierNews(newsDao);
	
	    	/* Modificatio nde la news et récupération de la news pour pouvoir la modifier ensuite */
	        News news = form.modifierNews(request);
	        
	        /* Si la news n'est pas nulle alors modification possible de la news, sinon affichage de l'admin accueil */
	        if (news != null)
	        {
	        	request.setAttribute( Helper.ATT_NEWS, news );
	        	this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_MODIFIER_NEWS ).forward( request, response );
	        }
	        else
	          this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_ACCUEIL ).forward( request, response ); 
    	}
    }
}
