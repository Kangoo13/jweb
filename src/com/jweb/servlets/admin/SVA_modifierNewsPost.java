package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsDao;

import com.jweb.forms.admin.FMA_modifierNewsPost;

import com.jweb.utils.Helper;

@WebServlet(value="/admin/modifierNewsPost", name="modifierNewsPost")
public class SVA_modifierNewsPost extends HttpServlet {

	private static final long serialVersionUID = -2866288029215094264L;

	private NewsDao     newsDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO News */
        this.newsDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsDao();
    }
    
   
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else{
    		/* Création de l'objet formulaire modifier news */
	    	FMA_modifierNewsPost form = new FMA_modifierNewsPost(newsDao);
	    	
	    	/* Modification de la news */
	        form.modifierNews(request);
	        
	        /* Affichage des news */
	        this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_NEWS ).forward( request, response );
	    	}
    	}
}
