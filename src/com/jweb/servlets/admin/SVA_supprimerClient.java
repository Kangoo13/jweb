package com.jweb.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOFactory;

import com.jweb.forms.admin.FMA_supprimerClient;

import com.jweb.utils.Helper;

@WebServlet(value="/admin/supprimerClient", name="supprimerClient")
public class SVA_supprimerClient extends HttpServlet {

	private static final long serialVersionUID = 7108296319930874391L;

	private ClientDao     clientDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Client */
        this.clientDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getClientDao();
    }
        
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* Création de l'objet formulaire supprimer client */
    		FMA_supprimerClient form = new FMA_supprimerClient(clientDao);

    		/* Suppression du client */
    		form.supprimerClient(request);
    		
    		/* Affichage des clients */
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_CLIENTS ).forward( request, response );
    	}
    }
}
