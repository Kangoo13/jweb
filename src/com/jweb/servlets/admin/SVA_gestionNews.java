package com.jweb.servlets.admin;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.News;

import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsDao;

import com.jweb.forms.admin.FMA_gestionNews;

import com.jweb.utils.Helper;

@WebServlet(value="/admin/gestionNews", name="gestionNews")
public class SVA_gestionNews extends HttpServlet {

	private static final long serialVersionUID = 8994473701928815164L;

	private NewsDao     newsDao;

    public void init() throws ServletException {
        /* R�cup�ration d'une instance de notre DAO News */
        this.newsDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsDao();
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* R�cup�ration des news */
            Map<Long, News> mapNews = new FMA_gestionNews(newsDao).recupererNews(request);	
            
            /* Ajout des news � la request */
            request.setAttribute( Helper.ATT_NEWS, mapNews );
            
            /* Affiche de la liste des news */
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_LISTER_NEWS ).forward( request, response );
    	}
	}

    
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	if (Helper.isAdmin(request) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
    		/* R�cup�ration des news */
            Map<Long, News> mapNews = new FMA_gestionNews(newsDao).recupererNews(request);	
            
            /* Ajout des news � la request */
            request.setAttribute( Helper.ATT_NEWS, mapNews );
            
            /* Affiche de la liste des news */
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_LISTER_NEWS ).forward( request, response );
    	}
    }
}
