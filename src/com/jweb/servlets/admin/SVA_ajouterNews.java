package com.jweb.servlets.admin;

import java.io.IOException;





import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jweb.beans.Client;
import com.jweb.dao.DAOFactory;
import com.jweb.dao.NewsDao;
import com.jweb.forms.admin.FMA_ajouterNews;
import com.jweb.utils.Helper;

@WebServlet(value="/admin/ajouterNews", name="ajouterNews")
public class SVA_ajouterNews extends HttpServlet {

	private static final long serialVersionUID = 4731259533492127852L;
	
	private NewsDao     newsDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO News*/
        this.newsDao = ( (DAOFactory) getServletContext().getAttribute( Helper.CONF_DAO_FACTORY ) ).getNewsDao();
    }
    
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	Client admin;
    	if ((admin = Helper.isAdmin(request)) == null)
    		this.getServletContext().getRequestDispatcher( Helper.VUE_ACCUEIL ).forward( request, response );
    	else
    	{
        	/* Préparation de l'objet formulaire */
            FMA_ajouterNews form = new FMA_ajouterNews(newsDao, admin);
            
            /* Ajout de la news */
            form.ajouterNews(request);
            
            /* Affichage de la liste des news */
            this.getServletContext().getRequestDispatcher( Helper.VUE_ADMIN_NEWS ).forward( request, response );
    	}

    }
}
