package com.jweb.forms;

import com.jweb.beans.Produit;
import com.jweb.dao.ProduitDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;

public final class FM_listerProduits {

    private ProduitDao			produitDao;
    
    public FM_listerProduits ( ProduitDao produitDao)
    {
    	this.produitDao = produitDao;
    }

    public Map<Long, Produit> recupererProduits( HttpServletRequest request ) {
    	try {
    		List<Produit> listeProduits = produitDao.lister();
            Map<Long, Produit> mapProduits = new HashMap<Long, Produit>();
            for ( Produit produit : listeProduits ) {
                mapProduits.put( produit.getId(), produit );
            }
    		return mapProduits;
    	}
        catch (DAOException e)
        {
        	e.printStackTrace();
        	return null;
        }


    }

}
