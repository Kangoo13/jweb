package com.jweb.forms;

import com.jweb.dao.ClientDao;
import com.jweb.dao.CommentaireDao;
import com.jweb.dao.ProduitDao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.jweb.beans.Commentaire;
import com.jweb.beans.Produit;

public final class FM_trouverProduit {
    private static final String CHAMP_ID     = "id";
    
    private ProduitDao			produitDao;
    private CommentaireDao		commentaireDao;
    private ClientDao			clientDao;
    
    public FM_trouverProduit ( ProduitDao ProduitDao, CommentaireDao CommentaireDao, ClientDao clientDao)
    {
    	this.produitDao = ProduitDao;
    	this.commentaireDao = CommentaireDao;
    	this.clientDao = clientDao;
    }

    public List<Produit> lister() {
        return produitDao.lister();
    }
    
    public Produit trouver(HttpServletRequest request)
    {
    	String id_s = getValeurChamp(request, CHAMP_ID);
    	if (id_s != null)
    	{
    	long id = Long.valueOf(id_s).longValue();
    	return produitDao.trouver(id);
    	}
    	return null;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

	public List<Commentaire> trouverCommentaires(HttpServletRequest request) {
    	String id_s = getValeurChamp(request, CHAMP_ID);
    	List<Commentaire> commentaires = null;
    	if (id_s != null)
    	{
	    	long id = Long.valueOf(id_s).longValue();
	    	commentaires = commentaireDao.listParIdProduit(id);
	        for ( Commentaire commentaire : commentaires ) {
	            commentaire.setClient(clientDao.trouver(commentaire.getIdMembre()));
	        }
    	}
		return commentaires;
	}
}
