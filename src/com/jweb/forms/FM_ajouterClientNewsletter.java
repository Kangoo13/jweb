package com.jweb.forms;

import com.jweb.dao.NewsletterDao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;
import com.jweb.utils.Helper;

public final class FM_ajouterClientNewsletter {
   
    private String              resultat;
    private Map<String, String> erreurs         = new HashMap<String, String>();
    private NewsletterDao			newsletterDao;
    
    public FM_ajouterClientNewsletter ( NewsletterDao NewsletterDao)
    {
    	this.newsletterDao = NewsletterDao;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public String getResultat() {
        return resultat;
    }

    public void ajouterNewsletter( HttpServletRequest request ) {
        String email = getValeurChamp( request, Helper.CHAMP_EMAIL );
        if (newsletterDao.trouver(email) == true)
        	setErreur(Helper.CHAMP_EMAIL, "Cette email est d�j� dans la base de donn�e");
        try {
            if ( erreurs.isEmpty() ) {
            	newsletterDao.ajouter(email);
                resultat = "Succ�s de l'ajout � la newsletter.";
            } else {
                resultat = "�chec de l'ajout � la newsletter.";
            }
        }
        catch (DAOException e)
        {
            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        }

        return;
    }

    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
