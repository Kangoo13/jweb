package com.jweb.forms.admin;

import com.jweb.dao.NewsDao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;
import com.jweb.utils.Helper;

public final class FMA_modifierNewsPost {
    
    private String              resultat;
    private Map<String, String> erreurs         = new HashMap<String, String>();
    private NewsDao			newsDao;
    
    public FMA_modifierNewsPost ( NewsDao newsDao)
    {
    	this.newsDao = newsDao;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public String getResultat() {
        return resultat;
    }

    public boolean modifierNews( HttpServletRequest request ) {
    	String id_s = getValeurChamp(request, Helper.CHAMP_ID);
    	String contenu = getValeurChamp(request, Helper.CHAMP_CONTENU);
    	String titre = getValeurChamp(request, Helper.CHAMP_TITRE);
    	
    	if (id_s != null && contenu != null && titre != null)
    	{
	    	long id = Long.valueOf(id_s).longValue();
	        if (newsDao.existe(id) == false)
	        	return false;
	        try {
	        	newsDao.modifier(id, contenu, titre);
	        }
	        catch (DAOException e)
	        {
	            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
	            e.printStackTrace();
	        	return false;
	        }
    	}
        return true;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

}
