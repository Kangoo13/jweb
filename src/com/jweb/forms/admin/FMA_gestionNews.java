package com.jweb.forms.admin;

import com.jweb.beans.News;
import com.jweb.dao.NewsDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;

public final class FMA_gestionNews {

    private NewsDao			newsDao;
    
    public FMA_gestionNews ( NewsDao newsDao)
    {
    	this.newsDao = newsDao;
    }

    public Map<Long, News> recupererNews( HttpServletRequest request ) {
    	try {
    		List<News> listeNews = newsDao.lister();
            Map<Long, News> mapNews = new HashMap<Long, News>();
            for ( News news : listeNews ) {
                mapNews.put( news.getId(), news );
            }
    		return mapNews;
    	}
        catch (DAOException e)
        {
        	e.printStackTrace();
        	return null;
        }


    }

}
