package com.jweb.forms.admin;

import com.jweb.beans.News;
import com.jweb.dao.NewsDao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;
import com.jweb.utils.Helper;

public final class FMA_modifierNews {
    
    private String              resultat;
    private Map<String, String> erreurs         = new HashMap<String, String>();
    private NewsDao			newsDao;
    
    public FMA_modifierNews ( NewsDao newsDao)
    {
    	this.newsDao = newsDao;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public String getResultat() {
        return resultat;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

	public News modifierNews(HttpServletRequest request) {
    	String id_s = getValeurChamp(request, Helper.CHAMP_ID);
    	
    	if (id_s != null)
    	{
	    	long id = Long.valueOf(id_s).longValue();
	        try {
	        	News news = newsDao.trouver(id);
	            return news;
	        }
	        catch (DAOException e)
	        {
	            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
	            e.printStackTrace();
	        	return null;
	        }
    	}


        return null;
	}
}
