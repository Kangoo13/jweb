package com.jweb.forms.admin;

import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;

public final class FMA_gestionClients {

    private ClientDao			clientDao;
    
    public FMA_gestionClients ( ClientDao clientDao)
    {
    	this.clientDao = clientDao;
    }

    public Map<Long, Client> recupererClients( HttpServletRequest request ) {
    	try {
    		List<Client> listeClients = clientDao.lister();
            Map<Long, Client> mapClients = new HashMap<Long, Client>();
            for ( Client client : listeClients ) {
                mapClients.put( client.getId(), client );
            }
    		return mapClients;
    	}
        catch (DAOException e)
        {
        	e.printStackTrace();
        	return null;
        }


    }

}
