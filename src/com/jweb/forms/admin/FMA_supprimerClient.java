package com.jweb.forms.admin;

import com.jweb.dao.ClientDao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;
import com.jweb.utils.Helper;

public final class FMA_supprimerClient {

    private String              resultat;
    private Map<String, String> erreurs         = new HashMap<String, String>();
    private ClientDao			clientDao;
    
    public FMA_supprimerClient ( ClientDao clientDao)
    {
    	this.clientDao = clientDao;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public String getResultat() {
        return resultat;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

	public boolean supprimerClient(HttpServletRequest request) {
    	String id_s = getValeurChamp(request, Helper.CHAMP_ID);
    	if (id_s != null)
    	{
    	long id = Long.valueOf(id_s).longValue();
        if (clientDao.existe(id) == false)
        	return false;
        try {
        	clientDao.supprimer(id);
        }
        catch (DAOException e)
        {
            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        	return false;
        }
    	}
        return true;
	}
}
