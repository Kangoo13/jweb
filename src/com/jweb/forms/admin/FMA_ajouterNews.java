package com.jweb.forms.admin;

import com.jweb.beans.Client;
import com.jweb.dao.NewsDao;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;
import com.jweb.utils.Helper;

public final class FMA_ajouterNews {

    private String              resultat;
    private Map<String, String> erreurs         = new HashMap<String, String>();
    private NewsDao			newsDao;
    private Client			client;
    
    public FMA_ajouterNews ( NewsDao NewsDao, Client client)
    {
    	this.newsDao = NewsDao;
    	this.client = client;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public String getResultat() {
        return resultat;
    }

    public boolean ajouterNews( HttpServletRequest request ) {
    	String titre = getValeurChamp(request, Helper.CHAMP_TITRE);
    	String contenu = getValeurChamp(request, Helper.CHAMP_CONTENU);
        try {
        	if (titre != null && contenu != null)
        	{
            	newsDao.creer(titre, contenu, client.getNom());
        	}
        	else
        	  return false;
        }
        catch (DAOException e)
        {
            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        	return false;
        }


        return true;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
