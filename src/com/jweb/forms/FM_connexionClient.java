package com.jweb.forms;


import javax.servlet.http.HttpServletRequest;

import com.jweb.utils.*;
import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOException;

public final class FM_connexionClient {
	
    private String              resultat = "";
    private ClientDao			clientDao;
    
    public FM_connexionClient ( ClientDao clientDao)
    {
    	this.clientDao = clientDao;
    }

    public String getResultat() {
        return resultat;
    }

    public Client creerClient( HttpServletRequest request ) {
        String password = getValeurChamp( request, Helper.CHAMP_PASSWORD );
        String email = getValeurChamp( request, Helper.CHAMP_EMAIL );

        Client client = new Client();
    	if (password == null || email == null)
    		resultat = "Un champ n'a pas �t� rempli";
    	else
    		try
        {
        	client = clientDao.trouver(email);
        	if (client == null || !Helper.MD5(password).matches(client.getPassword()))
        		resultat = "Echec lors de la connexion (mauvais email/mot de passe)";
        }
        catch (DAOException e)
        {
            resultat = "�chec de la connexion : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        }
         if ( resultat.matches("") ) {
             resultat = "Succ�s de la cr�ation du client.";
         } else {
             resultat += "<br />�chec de la cr�ation du client.";
         }
        return client;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

}
