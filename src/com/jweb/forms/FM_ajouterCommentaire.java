package com.jweb.forms;

import com.jweb.beans.Client;
import com.jweb.beans.Commentaire;
import com.jweb.dao.CommentaireDao;

import javax.servlet.http.HttpServletRequest;

import com.jweb.dao.DAOException;
import com.jweb.utils.Helper;

public final class FM_ajouterCommentaire {
    
    private CommentaireDao			commentaireDao;
    
    public FM_ajouterCommentaire ( CommentaireDao CommentaireDao)
    {
    	this.commentaireDao = CommentaireDao;
    }


    public boolean ajouterCommentaire( HttpServletRequest request ) {
    	String id_s = getValeurChamp(request, Helper.CHAMP_ID);
    	String note = getValeurChamp(request, Helper.CHAMP_NOTE);
    	String titre = getValeurChamp(request, Helper.CHAMP_TITRE);
    	String contenu = getValeurChamp(request, Helper.CHAMP_CONTENU);
    	if (id_s != null)
    	{
	    	long id = Long.valueOf(id_s).longValue();
	        if (commentaireDao.trouver(id, request.getSession().getAttribute("user")) == true)
	        	return false;
	        try {
		        	if (note != null && titre != null && contenu != null)
		        	{
		        		Commentaire commentaire = new Commentaire();
		        		Client client = (Client)request.getSession().getAttribute("user");
		        		commentaire.setIdProduit(id);
		        		commentaire.setDescription(contenu);
		        		commentaire.setTitre(titre);
		        		commentaire.setNote(Integer.valueOf(note));
		        		commentaire.setIdMembre(client.getId());
		            	commentaireDao.creer(commentaire);
		        	}
	        	}
	        	catch (DAOException e)
	        	{
	        		e.printStackTrace();
	        		return false;
	        	}
    	}
        return true;
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
