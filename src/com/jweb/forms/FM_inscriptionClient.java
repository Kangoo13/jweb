package com.jweb.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.jweb.utils.*;
import com.jweb.beans.Client;
import com.jweb.dao.ClientDao;
import com.jweb.dao.DAOException;

public final class FM_inscriptionClient {
   
    private String              resultat;
    private Map<String, String> erreurs         = new HashMap<String, String>();
    private ClientDao			clientDao;
    
    public FM_inscriptionClient ( ClientDao clientDao)
    {
    	this.clientDao = clientDao;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public String getResultat() {
        return resultat;
    }

    public Client creerClient( HttpServletRequest request ) {
        String nom = getValeurChamp( request, Helper.CHAMP_NOM );
        String prenom = getValeurChamp( request, Helper.CHAMP_PRENOM );
        String password = getValeurChamp( request, Helper.CHAMP_PASSWORD );
        String passwordc = getValeurChamp( request, Helper.CHAMP_PASSWORDC );
        String cp = getValeurChamp( request, Helper.CHAMP_CP );
        String pays = getValeurChamp( request, Helper.CHAMP_PAYS );
        String adresse = getValeurChamp( request, Helper.CHAMP_ADRESSE );
        String telephone = getValeurChamp( request, Helper.CHAMP_TELEPHONE );
        String email = getValeurChamp( request, Helper.CHAMP_EMAIL );

        Client client = new Client();

        try {
            validationNom( nom );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_NOM, e.getMessage() );
        }
        client.setNom( nom );

        try {
            validationPrenom( prenom );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_PRENOM, e.getMessage() );
        }
        client.setPrenom( prenom );

        try {
            validationAdresse( adresse );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_ADRESSE, e.getMessage() );
        }
        client.setAdresse( adresse );

        try {
            validationTelephone( telephone );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_TELEPHONE, e.getMessage() );
        }
        client.setTelephone( telephone );

        try {
            validationPassword( password, passwordc );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_PASSWORD, e.getMessage() );
        }

        client.setPassword( Helper.MD5(password) );
        
        try {
            validationCP( cp );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_CP, e.getMessage() );
        }
        client.setCp( cp );
        try {
            validationPays( pays );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_PAYS, e.getMessage() );
        }
        client.setPays( pays );
        
        try {
            validationEmail( email );
        } catch ( Exception e ) {
            setErreur( Helper.CHAMP_EMAIL, e.getMessage() );
        }
        client.setEmail( email );
        try {
            if ( erreurs.isEmpty() ) {
            	clientDao.creer(client);
                resultat = "Succ�s de la cr�ation du client.";
            } else {
                resultat = "�chec de la cr�ation du client.";
            }
        }
        catch (DAOException e)
        {
            resultat = "�chec de l'inscription : une erreur impr�vue est survenue, merci de r�essayer dans quelques instants.";
            e.printStackTrace();
        }

        return client;
    }

    private void validationNom( String nom ) throws Exception {
        if ( nom != null ) {
            if ( nom.length() < 2 ) {
                throw new Exception( "Le nom d'utilisateur doit contenir au moins 2 caract�res." );
            }
        } else {
            throw new Exception( "Merci d'entrer un nom d'utilisateur." );
        }
    }
    
    private void validationPassword( String password, String passwordc ) throws Exception {
        if ( password != null && passwordc != null) {
        	if (password.matches(passwordc))
        	{
        		if ( !password.matches("(?=.*\\d)(?=.*[a-z]).{6,20}")) {
        			throw new Exception( "Le mot de passe doit contenir un chiffre, une lettre et doit �tre de 6 � 20 caract�res." );
            }
        	}
        	else
        	{
    			throw new Exception( "Les mots de passe ne coincident pas." );
        	}
        }else {
            throw new Exception( "Merci d'entrer un password." );
        }
    }
    private void validationCP( String cp ) throws Exception {
        if ( cp != null ) {
            if ( !cp.matches( "^\\d+$" ) ) {
                throw new Exception( "Le code postal doit uniquement contenir des chiffres." );
            } else if ( cp.length() < 5 ) {
                throw new Exception( "Le code postal doit contenir au moins 5 chiffre." );
            }
        } else {
            throw new Exception( "Merci d'entrer un code postal." );
        }
    }
    private void validationPays( String pays ) throws Exception {
        if ( pays != null ) {
            if ( pays.length() < 2 ) {
                throw new Exception( "Le pays doit contenir au moins 2 caract�res." );
            }
        } else {
            throw new Exception( "Merci d'entrer un pays." );
        }
    }

    private void validationPrenom( String prenom ) throws Exception {
        if ( prenom != null && prenom.length() < 2 ) {
            throw new Exception( "Le pr�nom d'utilisateur doit contenir au moins 2 caract�res." );
        }
    }

    private void validationAdresse( String adresse ) throws Exception {
        if ( adresse != null ) {
            if ( adresse.length() < 10 ) {
                throw new Exception( "L'adresse de livraison doit contenir au moins 10 caract�res." );
            }
        } else {
            throw new Exception( "Merci d'entrer une adresse de livraison." );
        }
    }

    private void validationTelephone( String telephone ) throws Exception {
        if ( telephone != null ) {
            if ( !telephone.matches( "^\\d+$" ) ) {
                throw new Exception( "Le num�ro de t�l�phone doit uniquement contenir des chiffres." );
            } else if ( telephone.length() != 10 ) {
                throw new Exception( "Le num�ro de t�l�phone doit contenir 10 chiffres." );
            }
        } else {
            throw new Exception( "Merci d'entrer un num�ro de t�l�phone." );
        }
    }

    private void validationEmail( String email ) throws Exception {
        if ( email != null && !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
            throw new Exception( "Merci de saisir une adresse mail valide." );
        }
        else if (clientDao.trouver(email) != null)
        	throw new Exception( "Adresse email d�j� �xistante" );
    }

    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
