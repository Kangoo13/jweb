package com.jweb.dao;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;




import java.util.ArrayList;
import java.util.List;

import com.jweb.beans.Produit;

import static com.jweb.dao.DAOUtilitaire.*;

public class ProduitDaoImpl implements ProduitDao {
	private static final String SQL_SELECT_PAR_ID = "SELECT * FROM produits WHERE id = ?";
	private static final String SQL_SELECT_ALL = "SELECT * FROM produits";

	private DAOFactory          daoFactory;

    ProduitDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
    /* Impl�mentation de la m�thode trouver() d�finie dans l'interface ProduitDao */
    @Override
    public Produit trouver( long id ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Produit produit = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_ID, false, id );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
            	produit =  map( resultSet );
            	return produit;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return null;
    }
    

    /* Impl�mentation de la m�thode d�finie dans l'interface ProduitDao pour trouver un lister les produits */
    @Override
    public List<Produit> lister() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Produit> produits = new ArrayList<Produit>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement( SQL_SELECT_ALL );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	produits.add( map( resultSet ) );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }

        return produits;
    }
    
    /*
     * Simple m�thode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des produits (un
     * ResultSet) et un bean Produit.
     */
    private static Produit map( ResultSet resultSet ) throws SQLException {
    	Produit produit = new Produit();
    	produit.setId( resultSet.getLong( "id" ) );
    	produit.setName( resultSet.getString( "name" ) );
    	produit.setDescription( resultSet.getString( "description" ) );
    	produit.setImage( resultSet.getString( "image" ) );
    	produit.setPrice( resultSet.getFloat( "price" ) );
        return produit;
    }
}
