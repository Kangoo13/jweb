package com.jweb.dao;

import java.sql.ResultSet;

import com.jweb.beans.Client;
import com.jweb.beans.Commentaire;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.jweb.dao.DAOUtilitaire.*;

public class CommentaireDaoImpl implements CommentaireDao {
	private static final String SQL_SELECT_PAR_ID = "SELECT * FROM commentaires WHERE id_produit = ?";
	private static final String SQL_SELECT_PAR_CLIENT = "SELECT * FROM commentaires WHERE id_membre = ? AND id_produit = ?";
	private static final String SQL_INSERT = "INSERT INTO commentaires (id_produit, titre, note, id_membre, description, date) VALUES (?, ?, ?, ?, ?, NOW())";

	private DAOFactory          daoFactory;

    CommentaireDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    

    /* Impl�mentation de la m�thode creer() d�finie dans l'interface CommentaireDao */
    @Override
    public void creer( Commentaire commentaire ) throws IllegalArgumentException, DAOException {
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, false, commentaire.getIdProduit(), commentaire.getTitre(), commentaire.getNote(), commentaire.getIdMembre(), commentaire.getDescription() );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourn� par la requ�te d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la cr�ation du commentaire, aucune ligne ajout�e dans la table." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
    }

    /*
     * Simple m�thode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des commentaires (un
     * ResultSet) et un bean Commentaire.
     */
    private static Commentaire map( ResultSet resultSet ) throws SQLException {
    	Commentaire commentaire = new Commentaire();
    	commentaire.setDate(resultSet.getDate("date"));
    	commentaire.setDescription(resultSet.getString("description"));
    	commentaire.setTitre(resultSet.getString("titre"));
    	commentaire.setNote(resultSet.getInt("note"));
    	commentaire.setIdMembre(resultSet.getLong("id_membre"));
        return commentaire;
    }


    /* Lister les commentaires d'un produit par son id */
	@Override
	public List<Commentaire> listParIdProduit(long id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Commentaire> commentaires = new ArrayList<Commentaire>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connection, SQL_SELECT_PAR_ID, false, id);
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	commentaires.add( map( resultSet ) );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }

        return commentaires;
	}


    /* Impl�mentation de la m�thode trouver() d�finie dans l'interface CommentaireDao */
    @Override
    public boolean trouver( long id, Object user ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Client client = (Client)user;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_CLIENT, false, client.getId(), id );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
                return true;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return false;
    }
    
}
