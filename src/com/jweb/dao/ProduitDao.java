package com.jweb.dao;

import java.util.List;

import com.jweb.beans.Produit;

public interface ProduitDao {
    /*void ajouter( String email ) throws DAOException;

    void update( String email ) throws DAOException; */
    
    Produit trouver( long id ) throws DAOException;
    
    List<Produit> lister() throws DAOException;
}
