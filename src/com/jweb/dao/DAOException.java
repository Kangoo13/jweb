package com.jweb.dao;

public class DAOException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5159559749184992567L;

	/**
	 * 
	 */
	/*
     * Constructeurs
     */
    public DAOException( String message ) {
        super( message );
    }

    public DAOException( String message, Throwable cause ) {
        super( message, cause );
    }

    public DAOException( Throwable cause ) {
        super( cause );
    }
}
