package com.jweb.dao;

public class DAOConfigurationException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6993125387915378117L;

	/*
     * Constructeurs
     */
    public DAOConfigurationException( String message ) {
        super( message );
    }

    public DAOConfigurationException( String message, Throwable cause ) {
        super( message, cause );
    }

    public DAOConfigurationException( Throwable cause ) {
        super( cause );
    }
}
