package com.jweb.dao;

public interface NewsletterDao {
    void ajouter( String email ) throws DAOException;

    void update( String email ) throws DAOException;
    
    boolean trouver( String email ) throws DAOException;
}
