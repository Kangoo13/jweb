package com.jweb.dao;

import java.util.List;

import com.jweb.beans.Commentaire;

public interface CommentaireDao {
    void creer( Commentaire commentaire) throws DAOException;
    boolean trouver( long id, Object user) throws DAOException;

    List<Commentaire> listParIdProduit( long id ) throws DAOException;
}
