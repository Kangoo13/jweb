package com.jweb.dao;

import java.util.List;

import com.jweb.beans.News;

public interface NewsDao {
    void creer( News news ) throws DAOException;
    void supprimer(long id) throws DAOException;
    List<News> lister() throws DAOException;
	boolean existe(long id);
	News trouver(long id);
	void modifier(long id, String contenu, String titre);
	void creer(String titre, String contenu, String auteur);
}
