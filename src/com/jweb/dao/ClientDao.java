package com.jweb.dao;

import java.util.List;

import com.jweb.beans.Client;

public interface ClientDao {
    void creer( Client client ) throws DAOException;

    Client trouver( String email ) throws DAOException;
    Client trouver( long id ) throws DAOException;
	boolean existe(long id)throws DAOException;
	List<Client> lister()throws DAOException;

	void supprimer(long id) throws DAOException;

	void modifier(Client client);
}
