package com.jweb.dao;

import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



import static com.jweb.dao.DAOUtilitaire.*;

public class NewsletterDaoImpl implements NewsletterDao {
	private static final String SQL_SELECT_PAR_EMAIL = "SELECT * FROM newsletter WHERE email = ?";
	private static final String SQL_INSERT = "INSERT INTO newsletter (email, date) VALUES (?, NOW())";
	private static final String SQL_UPDATE = "UPDATE client SET newsletter = 1 WHERE email = ?";

	private DAOFactory          daoFactory;

    NewsletterDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
    /* Impl�mentation de la m�thode ajouter() d�finie dans l'interface NewsletterDao */
    @Override
    public void ajouter( String email ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, false, email );
            preparedStatement.executeUpdate();
            update(email);
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( preparedStatement, connexion );
        }

        return;
    }
    
    /* Impl�mentation de la m�thode trouver() d�finie dans l'interface NewsletterDao */
    @Override
    public boolean trouver( String email ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_EMAIL, false, email );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
                return true;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return false;
    }

    /* Impl�mentation de la m�thode update() d�finie dans l'interface NewsletterDao */
    @Override
    public void update( String email ) throws IllegalArgumentException, DAOException {
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE, false, email);
            preparedStatement.executeUpdate();
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
    }
}
