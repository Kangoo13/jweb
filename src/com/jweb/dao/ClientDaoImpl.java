package com.jweb.dao;

import java.sql.ResultSet;

import com.jweb.beans.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.jweb.dao.DAOUtilitaire.*;

public class ClientDaoImpl implements ClientDao {
	private static final String SQL_SELECT_PAR_EMAIL = "SELECT * FROM client WHERE email = ?";
	private static final String SQL_SELECT_PAR_ID = "SELECT * FROM client WHERE id = ?";
	private static final String SQL_INSERT = "INSERT INTO client (nom, prenom, adresse, telephone, email, password, cp, pays, date_inscription, newsletter, admin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, NOW(), 0, 0)";
	private static final String SQL_SELECT_ALL = "SELECT * from client";
	private static final String SQL_DELETE_PAR_ID = "DELETE FROM client WHERE id = ?";
	private static final String SQL_UPDATE_PAR_ID_SANS_PASSWORD = "UPDATE client SET nom = ?, prenom = ?, adresse = ?, telephone = ?, email = ?, cp = ?, pays = ?, newsletter = ?, admin = ? WHERE id = ?";
	private static final String SQL_UPDATE_PAR_ID_AVEC_PASSWORD = "UPDATE client SET nom = ?, prenom = ?, adresse = ?, telephone = ?, email = ?, password = ?, cp = ?, pays = ?, newsletter = ?, admin = ? WHERE id = ?";
	
	private DAOFactory          daoFactory;

    ClientDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
    /* Impl�mentation de la m�thode d�finie dans l'interface ClientDao pour supprimer un client*/
    @Override
    public void supprimer( long id ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE_PAR_ID, true, id );
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la suppression de la commande, aucune ligne supprim�e de la table." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
    }
    
    /* Impl�mentation de la m�thode d�finie dans l'interface ClientDao pour trouver un client selon l'email */
    @Override
    public Client trouver( String email ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Client client = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_EMAIL, false, email );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
                client = map( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return client;
    }
    

    /* Impl�mentation de la m�thode d�finie dans l'interface ClientDao pour trouver un client selon l'id */
    @Override
    public Client trouver( long id ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Client client = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_ID, false, id );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
                client = map( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return client;
    }

    /* Impl�mentation de la m�thode creer() d�finie dans l'interface ClientDao */
    @Override
    public void creer( Client client ) throws IllegalArgumentException, DAOException {
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, client.getNom(), client.getPrenom(), client.getAdresse(), client.getTelephone(), client.getEmail(), client.getPassword(), client.getCp(), client.getPays() );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourn� par la requ�te d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la cr�ation de l'utilisateur, aucune ligne ajout�e dans la table." );
            }
            /* R�cup�ration de l'id auto-g�n�r� par la requ�te d'insertion */
            valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            if ( valeursAutoGenerees.next() ) {
                /* Puis initialisation de la propri�t� id du bean Utilisateur avec sa valeur */
            	client.setId( valeursAutoGenerees.getLong( 1 ) );
            } else {
                throw new DAOException( "�chec de la cr�ation de l'utilisateur en base, aucun ID auto-g�n�r� retourn�." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
    }

    /*
     * Simple m�thode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des clients (un
     * ResultSet) et un bean Client.
     */
    private static Client map( ResultSet resultSet ) throws SQLException {
    	Client utilisateur = new Client();
        utilisateur.setId( resultSet.getLong( "id" ) );
        utilisateur.setAdmin( resultSet.getLong( "admin" ) );
        utilisateur.setNewsletter( resultSet.getLong( "newsletter" ) );
        utilisateur.setNom( resultSet.getString( "nom" ) );
        utilisateur.setPrenom( resultSet.getString( "prenom" ) );
        utilisateur.setAdresse( resultSet.getString( "adresse" ) );
        utilisateur.setTelephone( resultSet.getString( "telephone" ) );
        utilisateur.setEmail( resultSet.getString( "email" ) );
        utilisateur.setPassword( resultSet.getString( "password" ) );
        utilisateur.setDateInscription( resultSet.getDate("date_inscription" ));
        utilisateur.setPays( resultSet.getString("pays" ));
        utilisateur.setCp( resultSet.getString("cp" ));
        return utilisateur;
    }
    

    /* Impl�mentation de la m�thode d�finie dans l'interface ClientDao pour lister les clients */
	@Override
	public List<Client> lister() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Client> clients = new ArrayList<Client>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement( SQL_SELECT_ALL );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	clients.add( map( resultSet ) );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }

        return clients;
	}


    /* Impl�mentation de la m�thode d�finie dans l'interface ClientDao pour voir si un client existe */
	@Override
	public boolean existe(long id) throws DAOException{
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_ID, false, id );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
                return true;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return false;
	}


    /* Impl�mentation de la m�thode d�finie dans l'interface ClientDao pour modifier un client */
	@Override
	public void modifier(Client client) {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            if (client.getPassword() != null)
            	preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PAR_ID_AVEC_PASSWORD, true, client.getNom(), client.getPrenom(), client.getAdresse(), client.getTelephone(), client.getEmail(), client.getPassword(), client.getCp(), client.getPays(), client.getNewsletter(), client.getAdmin(), client.getId() );
            else
            	preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PAR_ID_SANS_PASSWORD, true, client.getNom(), client.getPrenom(), client.getAdresse(), client.getTelephone(), client.getEmail(), client.getCp(), client.getPays(), client.getNewsletter(), client.getAdmin(), client.getId() );     	
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la suppression de la commande, aucune ligne supprim�e de la table." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}
}
