package com.jweb.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.jweb.beans.News;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.jweb.dao.DAOUtilitaire.*;

public class NewsDaoImpl implements NewsDao {
	private DAOFactory          daoFactory;
	private static final String SQL_SELECT_ALL = "SELECT * from news";
	private static final String SQL_SELECT_PAR_ID = "SELECT * from news WHERE id = ?";
	private static final String SQL_INSERT = "INSERT INTO news (auteur, titre, date, texte_news) VALUES (?, ?, NOW(), ?)";
	private static final String SQL_DELETE_PAR_ID = "DELETE FROM news WHERE id = ?";
	private static final String SQL_UPDATE_PAR_ID = "UPDATE news SET titre = ?, texte_news = ? WHERE id = ?";
	
    NewsDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }
    
    /* Lister toutes les news */
    @Override
    public List<News> lister() throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<News> news = new ArrayList<News>();

        try {
            connection = daoFactory.getConnection();
            preparedStatement = connection.prepareStatement( SQL_SELECT_ALL );
            resultSet = preparedStatement.executeQuery();
            while ( resultSet.next() ) {
            	news.add( map( resultSet ) );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connection );
        }

        return news;
    }
    
    /* Impl�mentation de la m�thode supprimer d�finie dans l'interface NewsDao */
    @Override
    public void supprimer( long id ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_DELETE_PAR_ID, true, id );
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la suppression de la commande, aucune ligne supprim�e de la table." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
    }

    /* Impl�mentation de la m�thode creer() d�finie dans l'interface NewsDao */
    @Override
    public void creer( News news ) throws IllegalArgumentException, DAOException {
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, news.getAuteur(), news.getTitre(), news.getTexte() );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourn� par la requ�te d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la cr�ation de la news, aucune ligne ajout�e dans la table." );
            }
            /* R�cup�ration de l'id auto-g�n�r� par la requ�te d'insertion */
            valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            if ( valeursAutoGenerees.next() ) {
                /* Puis initialisation de la propri�t� id du bean Utilisateur avec sa valeur */
            	news.setId( valeursAutoGenerees.getLong( 1 ) );
            } else {
                throw new DAOException( "�chec de la cr�ation de l'utilisateur en base, aucun ID auto-g�n�r� retourn�." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
    }

    /*
     * Simple m�thode utilitaire permettant de faire la correspondance (le
     * mapping) entre une ligne issue de la table des news (un
     * ResultSet) et un bean News.
     */
    private static News map( ResultSet resultSet ) throws SQLException {
    	News news = new News();
    	news.setId( resultSet.getLong( "id" ) );
    	news.setAuteur( resultSet.getString( "auteur" ) );
    	news.setDate( resultSet.getDate( "date" ) );
    	news.setTexte( resultSet.getString( "texte_news" ) );
    	news.setTitre( resultSet.getString( "titre" ) );
        return news;
    }

    /* Savoir si une news existe ou non selon un ID */
	@Override
	public boolean existe(long id) {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_ID, false, id );
            resultSet = preparedStatement.executeQuery();
            /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
            if ( resultSet.next() ) {
                return true;
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
        	fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return false;
	}

	/* Trouver une news selon un ID et renvoyer le bean */
	@Override
	public News trouver( long id ) throws DAOException {
	   Connection connexion = null;
	   PreparedStatement preparedStatement = null;
	   ResultSet resultSet = null;
	   News news = null;

	   try {
	       /* R�cup�ration d'une connexion depuis la Factory */
	       connexion = daoFactory.getConnection();
	       preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_ID, false, id );
	       resultSet = preparedStatement.executeQuery();
	       /* Parcours de la ligne de donn�es de l'�ventuel ResulSet retourn� */
	       if ( resultSet.next() ) {
	           news = map( resultSet );
	       }
	   } catch ( SQLException e ) {
	       throw new DAOException( e );
	   } finally {
	    fermeturesSilencieuses( resultSet, preparedStatement, connexion );
	   }
	   return news;
	 }

	/* Modifier une news */
	@Override
	public void modifier(long id, String contenu, String titre) {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_UPDATE_PAR_ID, true, titre, contenu, id );
            int statut = preparedStatement.executeUpdate();
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la suppression de la commande, aucune ligne supprim�e de la table." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
	}

	/* Cr�er une news */
	@Override
	public void creer(String titre, String contenu, String auteur) {
    	Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;

        try {
            /* R�cup�ration d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, auteur, titre, contenu );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourn� par la requ�te d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "�chec de la cr�ation de la news, aucune ligne ajout�e dans la table." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
		
	}
}
